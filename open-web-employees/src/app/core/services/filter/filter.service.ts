import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FilterService {
  constructor () {}

  selectedSkills = [];
  employees: any[];
  filteredEmployees: any[];

  addSelectedSkills (skill) {
    if ( this.selectedSkills.indexOf(skill) === -1 ) { this.selectedSkills.push(skill); }
  }

  removeSelectedSkills (skill) {
    if ( this.selectedSkills.indexOf(skill) !== -1 ) { this.selectedSkills.splice(this.selectedSkills.indexOf(skill), 1); }
  }

  compareSelectedSkills (selectedSkills, employeeSkills) {
    return selectedSkills.every((val) => employeeSkills.includes(val));
  }

  getFilteredEmployees (value, employees) {
    (this.selectedSkills.indexOf(value) === -1) ? this.addSelectedSkills(value) : this.removeSelectedSkills(value);

    this.filteredEmployees = [];
    employees.forEach(employee => {
      if (this.compareSelectedSkills(this.selectedSkills, employee.skills)) {
        this.filteredEmployees.push(employee);
      }
    });

    return this.filteredEmployees;
  }
}
