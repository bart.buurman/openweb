export interface IEmployee {
    name: string;
    role: string;
    bio: string;
    skills: Array<string>;
    profileImage: string;
}
