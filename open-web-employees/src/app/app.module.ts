import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { EmployeeListComponent } from './modules/employee/employee-list/employee-list.component';
import { EmployeeService } from './core/services/employee/employee.service';

import { HttpClientModule } from '@angular/common/http';
import { ImagePreloadDirective } from './shared/directives/image-preload.directive';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    ImagePreloadDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
