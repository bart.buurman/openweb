import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/core/services/employee/employee.service';
import { FilterService } from 'src/app/core/services/filter/filter.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  providers: [FilterService]
})
export class EmployeeListComponent implements OnInit {

  public employees = [];
  public filteredEmployees = [];
  public skills = [];

  constructor(private _employeeService: EmployeeService, public filterService: FilterService) {}

  ngOnInit () {
    this.getEmployeesList();
  }

  filterEmployeesBy (skill) {
    this.filteredEmployees = this.filterService.getFilteredEmployees(skill, this.employees);
  }

  getEmployeesList (): void {
    this._employeeService.getEmployees().subscribe(
      (data: any) => {
        this.employees = this.filteredEmployees = data.employees;

        data.employees.forEach(employee => {
          employee.skills.forEach(skill => {
            if ( this.skills.indexOf(skill) === -1 ) { this.skills.push(skill); }
          });
        });
      }
    );
  }
}
